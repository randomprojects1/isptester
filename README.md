# ISP Tester

This program pings various IP addresses continually and records them in an sqlite3 database to measure ISP uptime and connectivity.

# Dependencies

1.  singularity
    https://sylabs.io/guides/3.5/user-guide/quick_start.html#quick-installation-steps

2.  python3 & pip
    python3: https://www.python.org/downloads/ (Or use your package manager of choice)
    pip: https://pip.pypa.io/en/stable/installing/ (Or use your package manager of choice)

3.  sregistry
    `pip install sregistry[all]`

# Example Usage
```
sregistry pull 479755928,randomprojects1/isptester
sudo singularity run --bind ~/Documents:/mnt/out $(sregistry get 479755928,randomprojects1/isptester) -d /mnt/out/test.db -t PING_RESULTS -i 8.8.8.8 8.8.4.4
```

The first line pulls the singularity file from job number 479755928. You can find this number by going https://gitlab.com/randomprojects1/isptester/pipelines, selecting a pipeline, then clicking Jobs.

The second line runs the code. 

* `sudo` is required to give the container the rights to ping. (There are other methods, this requires the least setup. See https://sylabs.io/guides/3.5/user-guide/security_options.html)
* `--bind ~/Documents:/mnt/out` allows the container to write to the ~/Documents folder on the host by writing to the /mnt/out folder in the container.
* `$(sregistry get 479755928,randomprojects1/isptester)` references the container we pulled from gitlab in the previous line.
* `-d /mnt/out/test.db` tells isptester.py to write to the database at `/mnt/out/test.db` (which is `~/Documents/test.db` on the host because of the bind option)
* `-t PING_RESULTS` tells isptester.py to write the data in the table named "PING_RESULTS" in the referenced database.
* `-i 8.8.8.8 8.8.4.4` tells isptester.py to ping google's DNS servers (8.8.8.8 and 8.8.4.4)
