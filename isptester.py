import sqlite3
from sqlite3 import Error
import subprocess
import argparse

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def validate_table(conn,table):
    """ 
    Verify the existance of, and create if necessary,
    the table in the given database file.
    :param conn: connection to the given database
    :param table: the name of the table to verify
    :return: integer error code
    """

    with conn:
        # Check for the table
        sql = 'SELECT sql FROM sqlite_master WHERE name="'+str(table)+'"'
        cur = conn.cursor()
        rows = cur.execute(sql)

        row = rows.fetchone()
        if row==None:
            print("Creating table: " + str(table))
            sql = "CREATE TABLE " + str(table) + " (dt DATETIME, ip VARCHAR(15), packetsize SMALLINT, success BOOL, ttl SMALLINT, pingtime FLOAT)"
            cur.execute(sql)
        else:
            if row[0][-94:] != "(dt DATETIME, ip VARCHAR(15), packetsize SMALLINT, success BOOL, ttl SMALLINT, pingtime FLOAT)":
                print("Error: Incorrectly formatted table.\nPlease select a different table name")
                return 1


    return 0


def create_pingrecord(conn, table, vals):
    """
    Create a new task
    :param conn: A Database connection object
    :param vals: A 5-tuple matching (ip,packetsize,success,ttl,pingtime)
    :return: The number of rows in the new database
    """

    sql = "INSERT INTO " + table + " (dt, ip, packetsize, success, ttl, pingtime) VALUES(STRFTIME('%Y-%m-%d %H:%M:%f','now'),?,?,?,?,?)"

    cur = conn.cursor()
    try:
        cur.execute(sql,vals)
    except Error as e:
        print(e)
        
    return cur.lastrowid

def ping(ip,size):
    """
    Uses ping system call to ping the supplied ip address with packets of the given size
    :param ip: IP address to ping
    :param size: The size of the ping packet
    :return: Values from system call
    """

    args = ["/bin/ping", "-c 1", "-s "+str(size), "-w 1", ip]
    pingresponse = subprocess.Popen(args,stdout=subprocess.PIPE).stdout.read()
    return str(pingresponse)

def pingparse(server,response):

    # Determine if the packet was received
    if len(response.split('---')[0].split('.')[-1])<=4:
        size = response.split('(')[2].split(')')[0]
        ttl = -1
        pingtime = -1
        success = 0
    else:
        # If ping is received
        responses = response.split('=')
        size = responses[0].split('n')[1].split(' ')[0]
        ttl = responses[2].split(' ')[0]
        pingtime = responses[3].split(' ')[0]
        success = 1
                    
    return (server,size,success,ttl,pingtime)

def main(database=None,table=None,ips=None):

    # Create database connection
    if database == None:
        database = input("Please enter the database: ")
    conn = create_connection(database)

    # Locate/Validate table
    if table == None:
        table = "isptest_results"
    if validate_table(conn,table) != 0:
        return 0

    if ips == None:
        ips = ["68.94.156.1","68.94.157.1","8.8.8.8","8.8.4.4"]
        
    while 1:
        with conn:
            for Server in ips:
                print("Pinging " + str(Server))

                # Ping the Server
                packetsize = 56
                response = ping(Server,packetsize)

                create_pingrecord(conn,table,pingparse(Server,response))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some database parameters')
    parser.add_argument('--database','-d', metavar='db_path', type=str, help='filepath and name of the database in which to log ping data')
    parser.add_argument('--table', '-t', metavar='tb_name', type=str, help='name of the table in which to store the ping data')
    parser.add_argument('--ips', '-i', metavar='IPs', type=str, nargs='+', help='ip addresses to ping')
    args = parser.parse_args()

    main(args.database,args.table,args.ips)
